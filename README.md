# Hermanos Macana

Es una comunidad de amigos que no teme a que le apaleen una idea, incluso mientras
está en desarrollo. El motivo no es el maltrato sino agregar puntos de vista divergentes.

La imágen de los hermanos macanas fue la primera que incorporamos a la página de 
Física Experimental II del Instituto Balseiro, Mariano Gómez Berisso y Willy Pregliasco.

La página Wiki documenta los proyectos que hacemos andar ahora en el laboratorio 
del IB.